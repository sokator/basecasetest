import curses
from random import randint
from copy import copy

from .elements import Element
from .shapes import SHAPES
from .utils import PIXEL

class Board:
    logic_borders = None
    logic_board = None
    stdscr = None
    old_elements = []
    curr_element = None
    dimension = 20

    def __init__(self, stdscr):
        self.stdscr = stdscr
        self.__init_borders()
        self.curr_element = self.__get_new_element()

    def __init_borders(self):
        self.logic_borders = [
            [True if x == 0 or x == self.dimension - 1 else False for x in range(self.dimension)] for y in range(self.dimension)
        ]
        self.logic_borders.append([True] * self.dimension)
        self.logic_board = copy(self.logic_borders)

    def __render_borders(self):
        [self.stdscr.addch(y, 0, PIXEL) for y in range(self.dimension)]
        [self.stdscr.addch(y, self.dimension-1, PIXEL) for y in range(self.dimension)]
        [self.stdscr.addch(self.dimension, x, PIXEL) for x in range(self.dimension)]

    def __get_new_element(self):
        return Element(self.stdscr, self.dimension, SHAPES[randint(0, len(SHAPES)-1)])

    def mov_x(self, val):
        self.curr_element.mov_x(val)

    def mov_y(self, val):
        self.curr_element.mov_y(val)

    def rotate(self, clockwise=True):
        self.curr_element.rotate(clockwise)

    def no_collisions(self):
        """Check if current element collides with anything on the board."""
        self.logic_board = copy(self.logic_borders)

        for elem in self.old_elements:
            elem.render_logic(self.logic_board)

        for y, row in enumerate(self.curr_element.test_logical_shape):
            for x, cell in enumerate(row):
                if not cell:
                    continue

                if cell and self.dimension <= self.curr_element.test_position.x+x:
                    return False

                if cell and self.dimension <= self.curr_element.test_position.y+y:
                    return False

                elif self.logic_board[self.curr_element.test_position.y+y][self.curr_element.test_position.x+x] and cell:
                    return False

        return True

    def commit_move(self):
        self.curr_element.commit_move()

    def revert_move(self):
        self.curr_element.revert_move()

    def __possible_move_check(self, possible_move):
        """Check if previous move is legal, and cleanup."""
        if possible_move:
            self.curr_element.revert_move()
            return True
        elif self.no_collisions():
            self.curr_element.revert_move()
            return True
        self.curr_element.revert_move()
        return False


    def possible_moves(self):
        """Check if there are no possible moves anymore."""

        possible_move = False
        self.mov_x(-1)
        self.mov_y(1)
        possible_move = self.__possible_move_check(possible_move)

        self.mov_x(1)
        self.mov_y(1)
        possible_move = self.__possible_move_check(possible_move)

        self.rotate(True)
        self.mov_y(1)
        possible_move = self.__possible_move_check(possible_move)

        self.rotate(False)
        self.mov_y(1)
        possible_move = self.__possible_move_check(possible_move)

        self.mov_y(1)
        possible_move = self.__possible_move_check(possible_move)

        return possible_move

    def spawn(self):
        self.old_elements.append(self.curr_element)
        self.curr_element = self.__get_new_element()
        if not self.no_collisions() or not self.possible_moves():
            self.render()
            return False
        return True

    def render(self):
        self.__render_borders()
        [elem.render() for elem in self.old_elements]
        self.curr_element.render()
        curses.curs_set(0)
