#written out in this way for ease of editing and clarity. When loaded into element object, gets converted to an array of arrays with
# boolean values.
SHAPES = [
    [
        '****',
    ],
    [
        '* ',
        '* ',
        '**'
    ],
    [
        ' *',
        ' *',
        '**'
    ],
    [
        ' *',
        '**',
        '* '
    ],
    [
        '**',
        '**',
    ]
]