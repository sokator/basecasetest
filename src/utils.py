PIXEL = '#'

def rotate_element(matrix, clockwise=True):
    """90 degrees either clockwise or not"""
    result = [[0 for y in range(len(matrix))] for x in range(len(matrix[0]))]

    # list comprehension this long would be incomprehensible
    if clockwise:
        for y in range(len(matrix)):
            for x in range(len(matrix[y])):
                result[x][y] = matrix[len(matrix) - y - 1][x]
    else:
        for y in range(len(matrix)):
            for x in range(len(matrix[y])):
                result[x][y] = matrix[y][len(matrix[0])-x-1]
    return result
