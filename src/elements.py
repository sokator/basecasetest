from copy import copy
from random import randint
from .utils import rotate_element, PIXEL


class Position:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Element:
    position = None
    stdscr = None
    logical_shape = None
    test_position = None
    test_logical_shape = None

    def __init__(self, stdscr, board_size, shape):
        self.stdscr = stdscr
        x = randint(1, board_size - len(shape[0])-1)
        self.position = Position(x, 0)
        self.test_position = Position(x, 0)
        self.__set_logical_shape(shape)

    def __set_logical_shape(self, shape):
        """Get the matrix of 'pixels' taken up by the element from the shape matrix."""
        self.logical_shape = []
        for row in shape:
            self.logical_shape.append([True if x == '*' else False for x in row])
        self.test_logical_shape = copy(self.logical_shape)

    def __un_render(self):
        """Element cleans up space it previously occupied on the screen"""
        for y, row in enumerate(self.logical_shape):
            for x, pixel in enumerate(row):
                if pixel:
                    self.stdscr.addch(y + self.position.y, x + self.position.x, ' ')

    def mov_x(self, val):
        self.test_position.x += val

    def mov_y(self, val):
        self.test_position.y += val

    def rotate(self, clockwise):
        self.test_logical_shape = rotate_element(self.logical_shape, clockwise)

    def commit_move(self):
        """Move has been checked as legal, so we can commit it."""
        self.__un_render()
        self.position = self.test_position
        self.logical_shape = self.test_logical_shape
        self.test_position = Position(self.position.x, self.position.y) # lets hope garbage collector does its job.

    def revert_move(self):
        """If move is illegal, revert."""
        self.test_position = copy(self.position)
        self.test_logical_shape = copy(self.logical_shape)
        self.test_position = copy(self.position) # lets hope garbage collector does its job.

    def render_logic(self, logic_board):
        """Paste cells taken up by the element into the board matrix."""
        for y, row in enumerate(self.logical_shape):
            for x, cell in enumerate(row):
                try:
                    logic_board[self.position.y+y][self.position.x+x] = cell
                except IndexError:
                    pass

    def render(self):
        for y, row in enumerate(self.logical_shape):
            for x, pixel in enumerate(row):
                if pixel:
                    self.stdscr.addch(y+self.position.y, x+self.position.x, PIXEL)
