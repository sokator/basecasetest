from .board import Board


class Game:
    board = None
    stdscr = None

    def __init__(self, stdscr):
        self.stdscr = stdscr
        self.stdscr.clear()
        self.board = Board(stdscr)

    def handle_user(self, player_key):
        if player_key == 'a':
            self.board.mov_x(-1)
        elif player_key == 'd':
            self.board.mov_x(1)
        elif player_key == 'w':
            self.board.rotate(False)
        elif player_key == 's':
            self.board.rotate(True)
        elif player_key == 'z':
            #added this, because being unable to just make the element go down was unbearable.
            pass
        self.board.mov_y(1)

        if self.board.no_collisions():
            self.board.commit_move()
        else:
            self.board.revert_move()

    def get_player_input(self):
        self.stdscr.addstr(22, 1, f'Press a button')
        player_key = self.stdscr.getkey()
        while player_key not in 'wsadz':
            self.stdscr.addstr(22, 1, f'Press a button - one of: w s a d z')
            player_key = self.stdscr.getkey()
        self.stdscr.addstr(23, 1, f'Pressed {player_key} - enter to confirm')
        confirm_key = self.stdscr.getkey()
        while confirm_key != '\n':
            self.stdscr.addstr(23, 1, f'Pressed {player_key} - enter to confirm')
            confirm_key = self.stdscr.getkey()
        return player_key

    def game_step(self):
        self.board.render()
        player_key = self.get_player_input()
        self.handle_user(player_key)
        if not self.board.possible_moves():
            if not self.board.spawn():
                return False
        return True

    def play(self):
        while self.game_step():
            pass

        self.stdscr.addstr(10, 8, f'GAME OVER')
        self.stdscr.addstr(11, 8, f'q to exit')
        player_key = self.stdscr.getkey()
        while player_key != 'q':
            player_key = self.stdscr.getkey()