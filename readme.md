Controls:  
• a (return): move piece left  
• d (return): move piece right  
• w (return): rotate piece counter clockwise  
• s (return): rotate piece clockwise  
• z (return): just go down - it was hard to play without this option  