from curses import wrapper
from src.game import Game


def main(stdscr):
    game = Game(stdscr)
    game.play()


if __name__ == '__main__':
    wrapper(main)